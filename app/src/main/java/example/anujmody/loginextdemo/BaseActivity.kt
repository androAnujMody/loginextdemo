package example.anujmody.loginextdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by playtm on 08/03/18.
 */
abstract class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())

        init()

    }

    abstract fun getLayout(): Int
    abstract fun init()


}