import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast


/**
 * Created by playtm on 05/01/18.
 */


internal fun View.hideIt() {
    this.visibility = View.GONE
}

internal fun View.showIt() {
    this.visibility = View.VISIBLE
}

internal fun Context.showAlert(msg: String) {
    var builder = AlertDialog.Builder(this)
    builder.setTitle("Invalid")
    builder.setMessage(msg)
    builder.setPositiveButton("OK") { dialog: DialogInterface?, which: Int ->
        dialog?.dismiss()
    }
    builder.show()
}

internal fun Context.showAlert(
    msg: String,
    oneButton: Boolean = false,
    posMsg: String,
    negMsg: String,
    onClick: (Boolean) -> Unit
) {
    var builder = AlertDialog.Builder(this)
    builder.setMessage(msg)
    if (oneButton) {
        builder.setPositiveButton(posMsg) { dialog: DialogInterface?, _: Int ->
            onClick(true)
            dialog?.dismiss()
        }
    } else {
        builder.setPositiveButton(posMsg) { dialog: DialogInterface?, which: Int ->
            onClick(true)
            dialog?.dismiss()
        }
        builder.setNegativeButton(negMsg) { dialog: DialogInterface?, which: Int ->
            onClick(false)
            dialog?.dismiss()
        }
    }

    builder.show()
}

val View.getContext: Context
    get() = context

fun ViewGroup.inflate(layoutId: Int): View {
    return LayoutInflater.from(context).inflate(layoutId, this, false)

}


fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}



