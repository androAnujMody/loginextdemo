package example.anujmody.loginextdemo.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.google.android.gms.location.LocationRequest
import io.reactivex.disposables.CompositeDisposable
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider


class LocationService : Service() {


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    private var disposable: CompositeDisposable? = null

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        super.onCreate()
        disposable = CompositeDisposable()
        var request =
                LocationRequest.create().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY).setInterval(20000)
        var locProvider = ReactiveLocationProvider(this)
        disposable?.add(locProvider.getUpdatedLocation(request).subscribe {

            //here once we get location we can broadcast it to calling activity to display lines on map

        })
    }



    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return Service.START_STICKY
    }

}