package example.anujmody.loginextdemo.adapters

import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import example.anujmody.loginextdemo.R
import inflate
import kotlinx.android.synthetic.main.list_item_gallery.view.*

class GalleryAdapter(private val context: Context, private val listImages: List<Uri>) :
    RecyclerView.Adapter<GalleryVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): GalleryVH {
        return GalleryVH(p0.inflate(R.layout.list_item_gallery), context)
    }

    override fun getItemCount(): Int = listImages.size

    override fun onBindViewHolder(p0: GalleryVH, p1: Int) {

        p0.displayImages(listImages[p1])
    }
}


class GalleryVH(itemView: View, var context: Context) : RecyclerView.ViewHolder(itemView) {

    fun displayImages(uri: Uri) {
        Glide.with(context).load(uri).into(itemView.iv_imageview)
    }

}

