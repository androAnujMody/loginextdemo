package example.anujmody.loginextdemo

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.LocationManager
import android.widget.Toast
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import example.anujmody.loginextdemo.service.LocationService
import example.anujmody.loginextdemo.ui.GalleryActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider
import ru.alexbykov.nopermission.PermissionHelper
import showToast


class MainActivity : BaseActivity(), OnMapReadyCallback {


    private lateinit var permissionHelper: PermissionHelper
    private lateinit var map: GoogleMap
    private var locationManager: LocationManager? = null
    private lateinit var disposable: CompositeDisposable

    override fun getLayout(): Int = R.layout.activity_main

    override fun init() {
        checkForPermission()

    }

    private fun checkForPermission() {
        permissionHelper = PermissionHelper(this)
        permissionHelper.check(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO

        ).withDialogBeforeRun(R.string.permissionTitle, R.string.permissionMsg, R.string.permissionOkMsg)
                .onSuccess(::permissionGranted).onDenied(::permissionDenied).onNeverAskAgain(::permissionNeverAskAgain)
                .run()
    }

    private fun permissionGranted() {

        displayMap()
        setUpView()

    }

    private fun setUpView() {
        bt_start_tracking.setOnClickListener {
            showToast("service started")
            startService(Intent(this, LocationService::class.java))
        }

        bt_gallery.setOnClickListener {
            startActivity(Intent(this, GalleryActivity::class.java))
        }
    }

    private fun displayMap() {

        disposable = CompositeDisposable()
        val mapFragment: SupportMapFragment? =
                supportFragmentManager.findFragmentById(R.id.g_map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

    }

    private fun permissionDenied() {
        Toast.makeText(this, "", Toast.LENGTH_SHORT).show()
    }

    private fun permissionNeverAskAgain() {
        permissionHelper.startApplicationSettingsActivity()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap ?: return
        with(googleMap) {
            getCuurentLocation(googleMap)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getCuurentLocation(googleMap: GoogleMap) {


        var request =
                LocationRequest.create().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY).setNumUpdates(1)
        var locProvider = ReactiveLocationProvider(this)
        disposable.add(locProvider.getUpdatedLocation(request).subscribe {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude, it.longitude), 15f))
        })


    }


}
