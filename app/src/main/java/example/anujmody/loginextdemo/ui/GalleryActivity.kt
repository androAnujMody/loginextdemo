package example.anujmody.loginextdemo.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraMetadata
import android.hardware.camera2.CaptureRequest
import android.media.Image
import android.net.Uri
import android.os.Environment
import android.support.v7.widget.GridLayoutManager
import example.anujmody.loginextdemo.BaseActivity
import example.anujmody.loginextdemo.R
import example.anujmody.loginextdemo.adapters.GalleryAdapter
import hideIt
import kotlinx.android.synthetic.main.activity_gallery.*
import me.aflak.ezcam.EZCam
import me.aflak.ezcam.EZCamCallback
import showAlert
import showIt
import top.defaults.camera.Photographer
import java.io.File
import java.io.FileOutputStream


class GalleryActivity : BaseActivity() {
    override fun getLayout(): Int = R.layout.activity_gallery

    private val SELECTPICTURES: Int = 100
    private lateinit var listImages: MutableList<Uri>
    private var previousCount: Int = 0
    private lateinit var adapter: GalleryAdapter
    private var photographer: Photographer? = null
    private var cam: EZCam? = null

    override fun init() {
        listImages = mutableListOf()
        rv_gallery.layoutManager = GridLayoutManager(this, 3)

        /*on click*/
        bt_take_picture.setOnClickListener {
            // photographer?.takePicture()
            cam?.takePicture()
        }

        fab_add.setOnClickListener {
            showAlert("Please select one ", false, "Camera", "Gallery") {

                if (it) {
                    openCamera()
                } else {
                    openIntent()
                }

            }
        }

    }


    private fun openCamera() {
        camera.showIt()
        fab_add.hideIt()
        cam = EZCam(this)
        val id = cam?.camerasList?.get(CameraCharacteristics.LENS_FACING_BACK) // should check if LENS_FACING_BACK exist before calling get()
        cam?.selectCamera(id)
        cam?.open(CameraDevice.TEMPLATE_PREVIEW, textureView)
        cam?.setCameraCallback(object : EZCamCallback {
            override fun onPicture(image: Image?) {
                saveImageToStorage(image)

            }

            override fun onCameraDisconnected() {

            }

            override fun onError(message: String?) {

            }

            override fun onCameraReady() {
                cam?.setCaptureSetting(CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE, CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY)
                cam?.startPreview()
            }
        })

    }

    private fun saveImageToStorage(image: Image?) {
        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/loginext")
        if (!myDir.exists()) {
            myDir.mkdirs()
        }
        val file = File(myDir, "${System.currentTimeMillis()}-image.jpg")
        try {
            val out = FileOutputStream(file)
            val buffer = image!!.planes[0].buffer
            val bytes = ByteArray(buffer.remaining())
            buffer.get(bytes)
            val myBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size, null)
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }


        if (listImages.isEmpty()) {
            listImages.add(Uri.fromFile(file))
            setAdapter()

        } else {
            listImages.add(Uri.fromFile(file))
            adapter.notifyDataSetChanged()
        }
        camera.hideIt()
        fab_add.show()
        cam?.stopPreview()
    }

    private fun openIntent() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECTPICTURES)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == SELECTPICTURES) {

            if (listImages.isNotEmpty()) {
                previousCount = listImages.size
            }

            if (data?.clipData != null) {

                for (index in 0 until data.clipData.itemCount) {
                    listImages.add(data.clipData.getItemAt(index).uri)

                    if (index == data.clipData.itemCount - 1) {

                        if (previousCount == 0) {
                            setAdapter()
                        } else {
                            //adapter.notifyItemRangeInserted(previousCount, listImages.size - previousCount)
                            adapter.notifyDataSetChanged()

                        }


                    }
                }

            } else if (data?.data != null) {
                listImages.add(data.data)
                if (previousCount == 0) {
                    setAdapter()
                } else {
                    adapter.notifyDataSetChanged()
                }
            }
        }

    }


    private fun setAdapter() {

        adapter = GalleryAdapter(this, listImages)
        rv_gallery.adapter = adapter
        rv_gallery.showIt()
        tv_add_from_gallery.hideIt()
    }


}